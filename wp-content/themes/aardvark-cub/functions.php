<?php

/** CubTheme File */
define('angry_parent_theme', get_template_directory( ));
define('angry_child_theme', get_stylesheet_directory( ));

require_once(angry_parent_theme."/Framework/Aardvark.php");
require_once(angry_parent_theme."/Framework/Child.php");

$Child = new AardvarkChild( );

//
//class AngryInit {
//
//    private $settings = array( );
//    private $settings_file = angry_child_theme . "/settings.json";
//
//    public function __construct( ){
//        add_action( 'after_switch_theme', array($this, 'init'));
//    }
//
//    function setup_theme($oldname, $oldtheme = false){
//
//    }
//
//    function save_settings( ){
//        $settings_file = fopen($this->settings_file, "w");
//        $settings_json = json_encode($this->settings);
//        fwrite($settings_file, $settings_json);
//        fclose($settings_file);
//    }
//
//    function init( ){
//        $this->prepare_settings( );
//        $this->save_settings( );
//        add_action( 'admin_enqueue_scripts', array($this, 'enqueue'));
//    }
//
//    function ajax_save_settings( ){
//
//    }
//
//    function get_settings( ){
//        return file_get_contents($this->settings_file); //json encoded
//    }
//
//    function prepare_settings( ){
//        $fileList = glob(angry_parent_theme . '/library/*');
//        foreach($fileList as $filename){
//            $path_parts = pathinfo($filename);
//            $key = strtolower($path_parts['filename']);
//            $name = ucwords(str_replace("-", ' ', $path_parts['filename']));
//            $this->settings[$key] = array('name' => $name, 'value' => false, 'path' => $filename);
//        }
//    }
//
//    function enqueue( ){
//        wp_register_style( 'swal-css', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.min.css', false, '1.0.0' );
//        wp_enqueue_style( 'swal-css' );
//        wp_enqueue_script( 'swal-script', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.all.min.js', array( ), '1.0.0', true);
//        wp_enqueue_script( 'swal-custom-reg-script', get_template_directory_uri( ) . '/dist/assets/js/app.js', array( ), '1.0.0', true);
//        wp_localize_script('swal-custom-reg-script', 'temp_object', array(
//           'ajax_url' => admin_url( 'admin-ajax.php' ),
//           'settings' =>  $this->get_settings( )
//        ));
//
//    }
//}
//$AngryInit = new AngryInit( );
//
//add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
//function my_theme_enqueue_styles() {
//
//    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
//
//    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
//    wp_enqueue_style( 'child-style',
//        get_stylesheet_directory_uri() . '/style.css',
//        array( $parent_style ),
//        wp_get_theme()->get('Version')
//    );
//}
//
////function angry_child_setup($oldname, $oldtheme=false) {
////
////    add_action( 'admin_enqueue_scripts', 'temp_admin_load');
////    echo is_dir(angry_parent_theme ) ? 'Hmmm' : 'NOOOO!';
////    $options = get_dirs(angry_parent_theme . '/library/*');
////    $myfile = fopen(angry_child_theme . "/settings.json", "w");
////    $settings_json = json_encode($options['settings']);
////    fwrite($myfile, $settings_json);
////    fclose($myfile);
////
////    $test = file_get_contents(angry_child_theme . "/settings.json");
////}
////add_action("after_switch_theme", "angry_child_setup", 10 ,  2);
//
////function get_dirs( $path = '.' ){
////    $options = array( );
////    $settings = array( );
////    $fileList = glob($path);
////    foreach($fileList as $filename){
////        $path_parts = pathinfo($filename);
////        $key = strtolower($path_parts['filename']);
////        $name = ucwords(str_replace("-", ' ', $path_parts['filename']));
////        $options[$key] = $name;
////        $settings[$key] = 0;
////    }
////    return array('options' => $options, 'settings' => $settings);
////}
////
////function
////
////function get_settings( ){
////    return file_get_contents(angry_child_theme . "/settings.json");
////}
////
////function temp_admin_load( ) {
////
////}
//
