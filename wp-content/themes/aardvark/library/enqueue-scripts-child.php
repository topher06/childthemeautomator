<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_child_scripts' ) ) :
	function foundationpress_child_scripts() {

		$current_theme = basename(get_permalink());
		// Enqueue the main Stylesheet.
		wp_enqueue_style( $current_theme . '-style', get_stylesheet_directory_uri() . '/dist/assets/css/' . foundationpress_asset_path( 'app.css' ), array(), get_theme_version(), 'all' );

		// Enqueue Founation scripts
		wp_enqueue_script( $current_theme . '-base', get_stylesheet_directory_uri() . '/dist/assets/js/' . foundationpress_asset_path( 'app.js' ), array( 'jquery' ), get_theme_version(), true );

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_child_scripts' );
endif;
