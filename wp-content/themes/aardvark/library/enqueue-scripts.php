<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

// Enqueue scripts for Parent and Child
locate_template( 'library/enqueue-scripts-parent.php', true );

// Enqueue scripts for Child only
if( is_child_theme() ) {
	locate_template( 'library/enqueue-scripts-child.php', true );
}