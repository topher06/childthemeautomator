<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

// Use the Parent theme's current version
if ( ! function_exists( 'get_theme_version' ) ) :
	function get_theme_version() {
		$theme_version = wp_get_theme( );
		return $theme_version = $theme_version->get( 'Version' );
	}
endif;

// Check to see if rev-manifest exists for CSS and JS static asset revisioning
//https://github.com/sindresorhus/gulp-rev/blob/master/integration.md
if ( ! function_exists( 'foundationpress_asset_path' ) ) :
	function foundationpress_asset_path( $filename ) {
		$filename_split = explode( '.', $filename );
		$dir            = end( $filename_split );
		$manifest_path  = dirname( dirname( __FILE__ ) ) . '/dist/assets/' . $dir . '/rev-manifest.json';

		if ( file_exists( $manifest_path ) ) {
			$manifest = json_decode( file_get_contents( $manifest_path ), true );
		} else {
			$manifest = [];
		}

		if ( array_key_exists( $filename, $manifest ) ) {
			return $manifest[ $filename ];
		}
		return $filename;
	}
endif;

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {
	// COMMENTED OUT CODE IS IN HERE:
	/*
		// Deregister the jquery version bundled with WordPress.
		// wp_deregister_script( 'jquery' );

		// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
		// wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1', false );


		// wp_enqueue_script( 'font-awesome-js', 'https://use.fontawesome.com/releases/v5.0.10/js/all.js', array( 'jquery' ), '5.0.10', true );
		// wp_enqueue_script( 'livechat-wiley', 'https://lptag.liveperson.net/tag/tag.js?site=25669575', array(), '1', true );

		// Enqueue FontAwesome from CDN. Uncomment the line below if you need FontAwesome.
		// wp_enqueue_script( 'fontawesome', 'https://use.fontawesome.com/5016a31c8c.js', array(), '4.7.0', true );

	*/

		// Enqueue the main Stylesheet.
		wp_enqueue_style( 'wiley', get_template_directory_uri() . '/dist/assets/css/' . foundationpress_asset_path( 'app.css' ), array(), get_theme_version(), 'all' );

		// Enqueue Font Awesome
		wp_enqueue_style( 'font-awesome-all', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css', array(), get_theme_version(), 'all' );

		// Enqueue Founation scripts
		wp_enqueue_script( 'foundation', get_template_directory_uri() . '/dist/assets/js/' . foundationpress_asset_path( 'app.js' ), array( 'jquery' ), get_theme_version(), true );

		// Add the comment-reply library on pages where it is necessary
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

// Google Fonts
function ecr_add_google_fonts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic,700italic', false, get_theme_version() );
}

add_action( 'wp_enqueue_scripts', 'ecr_add_google_fonts' );

