<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 9/23/2018
 * Time: 11:32 PM
 */

class Aardvark {

    private $parent_dir = false;
    private $parent_uri = false;
    private $child_dir = false;
    private $child_uri = false;
    private $settings_file = '';
    private $settings = array( );
    private $child_theme = false;

    public function __construct( ){

        //lets first check to see what theme we are using
        $this->child_theme = is_child_theme( ) ? true : false;
        $this->parent_dir = get_template_directory( );
        $this->parent_uri = get_template_directory_uri( );
        $this->child_dir = get_stylesheet_directory( );
        $this->child_uri = get_stylesheet_directory_uri( );
        $this->settings_file = $this->child_theme ? $this->child_dir . "/settings.json" : $this->parent_dir . "/settings.json";

    }
}