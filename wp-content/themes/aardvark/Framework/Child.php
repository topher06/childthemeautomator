<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 9/25/2018
 * Time: 10:17 PM
 */

class AardvarkChild extends Aardvark {

    private $parent_dir = false;
    private $parent_uri = false;
    private $child_dir = false;
    private $child_uri = false;
    private $settings_file = '';
    private $settings = array( );
    private $child_theme = false;

    public function __construct( ){
        $this->child_theme = is_child_theme( ) ? true : false;
        $this->parent_dir = get_template_directory( );
        $this->parent_uri = get_template_directory_uri( );
        $this->child_dir = get_stylesheet_directory( );
        $this->child_uri = get_stylesheet_directory_uri( );
        $this->settings_file = $this->child_theme ? $this->child_dir . "/settings.json" : $this->parent_dir . "/settings.json";
        //Checks for Theme Active
        add_action( 'after_switch_theme', array($this, 'init'));
    }

    public function init( ){
        $this->prepare( );
        $this->save( );
        add_action( 'admin_enqueue_scripts', array($this, 'enqueue'));
    }

    private function save( ){
        $settings_file = fopen($this->settings_file, "w");
        $settings_json = json_encode($this->settings);
        fwrite($settings_file, $settings_json);
        fclose($settings_file);
    }

    private function retrieve($json = false){
        $file = file_get_contents($this->settings_file); //json encoded
        return !$json ? json_decode($file) : $file;
    }

    private function prepare( ){
        //prepare settings used to get "features/includes"
        $fileList = glob(get_template_directory( ) . '/library/*');
        foreach($fileList as $filename){
            $path_parts = pathinfo($filename);
            $key = strtolower($path_parts['filename']);
            $name = ucwords(str_replace("-", ' ', $path_parts['filename']));
            $this->settings[$key] = array('name' => $name, 'value' => false, 'path' => $filename);
        }
    }

    public function enqueue( ){
        wp_register_style( 'swal-css', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.min.css', false, '1.0.0' );
        wp_enqueue_style( 'swal-css' );
        wp_enqueue_script( 'swal-script', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.all.min.js', array( ), '1.0.0', true);
        wp_enqueue_script( 'swal-custom-reg-script', get_template_directory_uri( ) . '/dist/assets/js/app.js', array( ), '1.0.0', true);
        wp_localize_script('swal-custom-reg-script', 'temp_object', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'settings' =>  $this->retrieve(true),
            'secure' => wp_create_nonce( "settingsToSave" )
        ));
    }
}